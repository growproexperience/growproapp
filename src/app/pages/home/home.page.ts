import {Component, OnInit} from '@angular/core';
import {MenuController} from '@ionic/angular';

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

    private nextEvents;
    private events;

    constructor(private menu: MenuController) {
    }

    ngOnInit() {
        this.nextEvents = [
            {
                name: 'Name of the amazing contest',
                date: '2020-09-01T23:28:56.782Z',
                location: 'Mr Gregory Park Club'
            },
            {
                name: 'Name of the event',
                date: '2020-10-01T20:00:56.782Z',
                location: 'Mr Gregory Park Club'
            }
        ];
        this.events = [
            {
                name: 'Event for unstopable dreamers',
                description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item, the content is so long that needs to be truncated to be displayed in a maximum of 3 lines',
                date: '2020-09-01T23:28:56.782Z',
                location: 'Mr Gregory Park Club',
                nbAttendees: 32,
                prize: '50 - 200 coins'
            },
            {
                name: 'Event for dreamers',
                description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item',
                date: '2020-09-01T23:28:56.782Z',
                location: 'Mr Gregory Park Club',
                nbAttendees: 1,
                prize: '10 - 20 coins'
            },
            {
                name: 'Event for dreamers',
                description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item',
                date: '2020-09-01T23:28:56.782Z',
                location: 'Mr Gregory Park Club',
                nbAttendees: 1,
                prize: '10 - 20 coins'
            },
            {
                name: 'Event for dreamers',
                description: 'This is a "Facebook" styled Card. The header is created from a Thumbnail List item',
                date: '2020-09-01T23:28:56.782Z',
                location: 'Mr Gregory Park Club',
                nbAttendees: 1,
                prize: '10 - 20 coins'
            },

        ];
    }

    openFirst() {
        this.menu.enable(true, 'first');
        this.menu.open('first');
    }
}
