import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.page.html',
    styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

    slideOpts = {
        initialSlide: 0,
        speed: 400
    };
    slides = [
        {
            image: '/assets/images/welcome-step-1.svg',
            title: 'welcome.slides.slide1.title',
            description: 'welcome.slides.slide1.description',
        },
        {
            image: '/assets/images/welcome-step-2.svg',
            title: 'welcome.slides.slide2.title',
            description: 'welcome.slides.slide2.description',
        },
        {
            image: '/assets/images/welcome-step-3.svg',
            title: 'welcome.slides.slide3.title',
            description: 'welcome.slides.slide3.description',
        },
        {
            image: '/assets/images/welcome-step-4.svg',
            title: 'welcome.slides.slide4.title',
            description: 'welcome.slides.slide4.description',
        },
        {
            image: '/assets/images/welcome-step-5.svg',
            title: 'welcome.slides.slide5.title',
            description: 'welcome.slides.slide5.description',
        },
    ];

    constructor() {
    }

    ngOnInit() {
    }

}
