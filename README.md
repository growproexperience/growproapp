#Prerequisites

- node >= v12.0
- ionic-cli >= 6.9.2`

#Installation
After cloning this repo to your local filesystem, go to the app directory an run:
````
npm install
````

#Develop

Run ionic serve within the app directory to see your app in the browser:
````
ionic serve
````